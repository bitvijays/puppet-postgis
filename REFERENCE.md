# Reference
<!-- DO NOT EDIT: This document was generated by Puppet Strings -->

## Table of Contents

**Classes**

* [`postgis`](#postgis): Installs PostGIS and manages the postgresql server instance.
* [`postgis::install`](#postgisinstall): manages the installation of postgis and configuration.

## Classes

### postgis

Postgresql is managed via the puppetlabs/postgresql module.

* **See also**
https://www.postgresql.org/docs/9.6/static/runtime-config-resource.html#GUC-MAX-WORKER-PROCESSES

#### Examples

##### 

```puppet
include postgis
```

#### Parameters

The following parameters are available in the `postgis` class.

##### `postgres_pass`

Data type: `String`

The password for the postgres user

Default value: 'pass'

##### `ipv4_acls`

Data type: `Optional`

The HPA IPV4 additional rules.

Default value: `undef`

##### `postgresql_version`

Data type: `String`

The version of postgresql to use.

Default value: '9.4'

##### `postgis_version`

Data type: `String`

The version of postgis.

Default value: '2.2'

##### `pg_hba_conf_defaults`

Data type: `Boolean`

Uses the default hba configuration.  To use the hba's defined outside
of this class, set to false.

Default value: `true`

##### `pg_hba_rules`

Data type: `Optional[Hash]`



Default value: `undef`

##### `max_connections`

Data type: `String`

The maximum number of client connections allowed

Default value: '100'

##### `max_worker_processes`

Data type: `Integer`

Sets the maximum number of background processes that the system can support.

Default value: 8

##### `max_parallel_workers_per_gather`

Data type: `Integer`

Sets the maximum number of workers that can be started by a
single Gather node.

Default value: 0

##### `shared_buffers`

Data type: `String`

Determines how much memory is dedicated to PostgreSQL to use for caching
data. A reasonable starting value for shared_buffers is 1/4 of the memory
in your system.

Default value: '128MB'

##### `effective_cache_size`

Data type: `Optional[String]`

An estimate of how much memory is available for disk caching by the
operating system and within the database itself, after taking into account
what's used by the OS itself and other applications.
Setting effective_cache_size to 1/2 of total memory would be a normal
conservative setting.

Default value: `undef`

##### `checkpoint_segments`

Data type: `String`

PostgreSQL writes new transactions to the database in files called WAL
segments that are 16MB in size. Every time checkpoint_segments worth of
these files have been written, by default 3, a checkpoint occurs.
Checkpoints can be resource intensive, and on a modern system doing one
every 48MB will be a serious performance bottleneck. Setting
checkpoint_segments to a much larger value improves that. Unless you're
running on a very small configuration, you'll almost certainly be better
setting this to at least 10, which also allows usefully increasing the
completion target.

Default value: '3'

##### `checkpoint_completion_target`

Data type: `String`

@see https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server

Default value: '0.5'

##### `enable_autovacuum`

Data type: `Boolean`

Enables the autovacuum process.
@see https://www.postgresql.org/docs/9.6/routine-vacuuming.html#AUTOVACUUM

Default value: `true`

##### `data_dir`

Data type: `Optional[String]`

Overrides the default PostgreSQL data directory for the target platform.

Default value: `undef`

### postgis::install

manages the installation of postgis and configuration.

