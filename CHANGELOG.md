# postgis puppet module changelog

## 2019-10-21 Version 0.3.5

- Removed package postgis due to dependency issues.

## 2019-10-02 Version 0.3.4

- Added postgis package.

## 2019-10-02 Version 0.3.3

- Updated the changelog formatting to comply with Markdown Linting.
- Added a parameter for disabling autovacuum.

## 2019-05-27 Version 0.3.2

- Added data_dir parameter to control the postgresql database directory.

## 2019-01-16 Version 0.3.1

- Removed all references to init_db.bash

## 2019-01-16 Version 0.3.0

- Removed this module from managing the version dependencies between postgis and postgresql.
- Added a parameter for setting the postgresql version.
- Removed anchor pattern as its no longer needed in puppet >4.x
- Converted to PDK compatible.

## 2018-11-01 Version 0.2.5

- Updated metadata's source url.
 
## 2018-11-01 Version 0.2.4

- Updated license in metadata.

## 2018-11-01 Version 0.2.3

- Updated metadata to include requirements.
- Removed debugging statements.

## 2018-10-26 Version 0.2.2

- Fixed an issue with max_parallel_workers_per_gather
 
## 2018-10-11 Version 0.2.1

- Updated changelog with missing doco.
- Ensured max_parallel_workers_per_gather are not present in pre-9.6 version configurations.

## 2018-10-11 Version 0.2.0

- Added a changelog.
- Removed max_parallel_workers_per_gather from postgres version 9.5.
- Fixed a bug where max_parallel_workers_per_gather was being applied to all versions of postgresql.
- Added Puppet Strings to documentation.
- Added reference document generated from puppet strings.
