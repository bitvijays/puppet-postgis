# @summary Installs PostGIS and manages the postgresql server instance.
# Postgresql is managed via the puppetlabs/postgresql module.
#
# @param postgres_pass
#   The password for the postgres user
#
# @param ipv4_acls
#   The HPA IPV4 additional rules.
#
# @param postgresql_version
#   The version of postgresql to use.
#
# @param postgis_version
#   The version of postgis.
#
# @param pg_hba_conf_defaults
#   Uses the default hba configuration.  To use the hba's defined outside
#   of this class, set to false.
#
# @param pg_hba_rules
#
# @param max_connections
#   The maximum number of client connections allowed
#
# @param max_worker_processes
#   Sets the maximum number of background processes that the system can support.
#
# @param max_parallel_workers_per_gather
#   Sets the maximum number of workers that can be started by a
#   single Gather node.
# @see https://www.postgresql.org/docs/9.6/static/runtime-config-resource.html#GUC-MAX-WORKER-PROCESSES
#
# @param shared_buffers
#   Determines how much memory is dedicated to PostgreSQL to use for caching
#   data. A reasonable starting value for shared_buffers is 1/4 of the memory
#   in your system.
#
# @param effective_cache_size
#   An estimate of how much memory is available for disk caching by the
#   operating system and within the database itself, after taking into account
#   what's used by the OS itself and other applications.
#   Setting effective_cache_size to 1/2 of total memory would be a normal
#   conservative setting.
#
# @param checkpoint_segments
#   PostgreSQL writes new transactions to the database in files called WAL
#   segments that are 16MB in size. Every time checkpoint_segments worth of
#   these files have been written, by default 3, a checkpoint occurs.
#   Checkpoints can be resource intensive, and on a modern system doing one
#   every 48MB will be a serious performance bottleneck. Setting
#   checkpoint_segments to a much larger value improves that. Unless you're
#   running on a very small configuration, you'll almost certainly be better
#   setting this to at least 10, which also allows usefully increasing the
#   completion target.
#
# @param checkpoint_completion_target
#   @see https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server
#
# @param enable_autovacuum
#   Enables the autovacuum process.
#   @see https://www.postgresql.org/docs/9.6/routine-vacuuming.html#AUTOVACUUM
#
# @param data_dir
#   Overrides the default PostgreSQL data directory for the target platform.
#
# @example
#   include postgis
#
class postgis (
  String           $postgres_pass                   = 'pass',
  Optional         $ipv4_acls                       = undef,
  Boolean          $pg_hba_conf_defaults            = true,
  Optional[Hash]   $pg_hba_rules                    = undef,
  String           $postgresql_version              = '9.4',
  String           $postgis_version                 = '2.2',
  String           $max_connections                 = '100',
  Integer          $max_worker_processes            = 8,
  Integer          $max_parallel_workers_per_gather = 0,
  String           $shared_buffers                  = '128MB',
  Optional[String] $effective_cache_size            = undef,
  String           $checkpoint_segments             = '3',
  String           $checkpoint_completion_target    = '0.5',
  Boolean          $enable_autovacuum               = true,
  Optional[String] $data_dir                        = undef,
) {
  # varaibles
  $root_dir = '/opt/postgis'
  $init_db  = "${root_dir}/init_db.bash"

  class {'postgis::install': }
}
