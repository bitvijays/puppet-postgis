# @summary manages the installation of postgis and configuration. 
#
class postgis::install {

  # Changing the datadir after installation causes the server to come to a full stop before making the change. 
  # On Ubuntu, you must explicitly set needs_initdb = true to allow Puppet to initialize the database in the new datadir
  # (needs_initdb defaults to true on other systems).
  class { 'postgresql::globals':
    manage_package_repo => true,
    version             => $postgis::postgresql_version,
    postgis_version     => $postgis::postgis_version,
    datadir             => $postgis::data_dir,
  }

  # setup the postgresql server
  class { 'postgresql::server':
    postgres_password    => $postgis::postgres_pass,
    listen_addresses     => '*',
    ipv4acls             => $postgis::ipv4_acls,
    pg_hba_conf_defaults => $postgis::pg_hba_conf_defaults,
    encoding             => 'UTF8',
    require              => Class['postgresql::globals'],
  }

  # check for hba_rules
  if $postgis::pg_hba_rules != undef {
    create_resources(postgresql::server::pg_hba_rule, $postgis::pg_hba_rules)
  }

  class { 'postgresql::server::postgis':
    require              => Class['postgresql::globals'],
  }
  #ensure_packages(['postgis'])

  # configure postgres.conf
  postgresql::server::config_entry { 'max_connections':
    value => $postgis::max_connections,
  }

  postgresql::server::config_entry { 'max_worker_processes':
    value => $postgis::max_worker_processes,
  }

  postgresql::server::config_entry { 'shared_buffers':
    value   => $postgis::shared_buffers,
    require => Postgresql::Server::Config_entry['max_connections'],
  }

  postgresql::server::config_entry { 'autovacuum':
    value   => "${postgis::enable_autovacuum}",
    require => Postgresql::Server::Config_entry['shared_buffers'],
  }

  case $postgis::postgresql_version {
    /^(9.1|9.2|9.3|9.4)$/: {
      postgresql::server::config_entry { 'checkpoint_segments':
        value   => $postgis::checkpoint_segments,
        require => Postgresql::Server::Config_entry['autovacuum'],
        before  =>
        Postgresql::Server::Config_entry['checkpoint_completion_target'],
      }
      postgresql::server::config_entry { 'max_parallel_workers_per_gather':
        ensure  => absent,
        require => Postgresql::Server::Config_entry['autovacuum'],
        before  =>
        Postgresql::Server::Config_entry['checkpoint_completion_target'],
      }
      package{'postgresql-9.5':
        ensure  => absent,
        require => Postgresql::Server::Config_entry['autovacuum'],
        before  =>
        Postgresql::Server::Config_entry['checkpoint_completion_target'],
      }
    }
    '9.5': {
      postgresql::server::config_entry { 'max_parallel_workers_per_gather':
        ensure  => absent,
        require => Postgresql::Server::Config_entry['autovacuum'],
        before  =>
        Postgresql::Server::Config_entry['checkpoint_completion_target'],
      }
    }
    default: {
      postgresql::server::config_entry { 'max_parallel_workers_per_gather':
        value => $postgis::max_parallel_workers_per_gather,
      }
    }
  }

  postgresql::server::config_entry { 'checkpoint_completion_target':
    value   => $postgis::checkpoint_completion_target,
    require => Postgresql::Server::Config_entry['autovacuum'],
  }

  if $postgis::effective_cache_size != undef {
    postgresql::server::config_entry { 'effective_cache_size':
      value   => $postgis::effective_cache_size,
      require =>
      Postgresql::Server::Config_entry['checkpoint_completion_target'],
    }
  }

  file {$postgis::root_dir:
    ensure  => directory,
    owner   => postgres,
    group   => postgres,
    require => Class['postgresql::server'],
  }
}
